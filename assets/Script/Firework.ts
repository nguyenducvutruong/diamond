
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

    }

    start () {
        var animation = this.node.getComponent(cc.Animation);
        animation.play();
    }

    // update (dt) {}
}
