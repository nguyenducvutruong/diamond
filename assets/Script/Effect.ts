
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    selfDestroy(){
        this.node.destroy();
    }

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.schedule(this.selfDestroy, 0.5);
    }

    // update (dt) {}
}
