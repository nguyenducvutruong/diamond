
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    scoreLabel: cc.Label = null;

    @property(cc.Label)
    timeLabel: cc.Label = null;

    @property
    maxTime: number = 5.4;

    public currentScore: number = 0;
    timeLeft = 0;
    isLoaded = false;


    public UpdateScore(score: number){
        this.currentScore += score;

        if(this.currentScore < 0){
            this.currentScore = 0;
        }

        this.scoreLabel.string = "00"+this.currentScore+" ĐIỂM";
    }
    UpdateTime(){
        this.timeLabel.string = "00:"+this.FormatTime(this.timeLeft);
    }

    FormatTime(time: number){
        
        if(time > 9.5){
            return time.toFixed();
        }
        else{
            return "0"+time.toFixed();
        }
    }

    //called when next scene is loaded
    UpdateScoreCallBack(){
        var gameMaster = cc.find("GameMaster");
        cc.game.removePersistRootNode(gameMaster);
        cc.find("Canvas/Holder/FrameScore/ScoreLabel").getComponent(cc.Label).string = 
            gameMaster.getComponent("GameMaster").currentScore.toString();
        //console.log("Score brought to this scene: "+gameMaster.getComponent("GameMaster").currentScore.toString());
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.parent = null;
        cc.game.addPersistRootNode(this.node);
        
        console.log("is :"+cc.game.addPersistRootNode(this.node));

        this.timeLeft = this.maxTime;
        this.scoreLabel.string = "00" + this.currentScore+" ĐIỂM";
        this.timeLabel.string = "00:"+this.FormatTime(this.timeLeft);
    }

    waveSpawner: any = null;

    start () {
        
        this.waveSpawner = this.getComponent("WaveSpawner");
        // cc.director.preloadScene("WinScene", function(){
        //    console.log("win scene preloaded");
           
        // });
    }



    update (dt) {
        //whether loaded into next scene
        if(this.isLoaded){
            return;
        }

        if (this.timeLeft <= 0.1)
        {
            this.timeLeft = this.maxTime;
            this.waveSpawner.stopAllSchedules();
            this.isLoaded = true;
            cc.director.loadScene("LoseScene", this.UpdateScoreCallBack);
            return;
        }

        if(this.currentScore >= 7){
            this.timeLeft = this.maxTime;
            this.waveSpawner.stopAllSchedules();
            this.isLoaded = true;
            cc.director.loadScene("WinScene", this.UpdateScoreCallBack);
            return;
        }
        
        this.timeLeft -= dt;
        this.UpdateTime();
    }



}
