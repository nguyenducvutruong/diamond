
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    logScore(){
        var gameMaster = cc.find("GameMaster");
        console.log("Current score: "+gameMaster.getComponent("GameMaster").currentScore.toString());
    }

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    // update (dt) {}
}
