
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    effect: cc.Prefab = null;

    @property
    score: number = 0;

    @property
    speed: number = 50;

    gameMaster: cc.Node = null;
    
    onMouseDown(event: cc.Event.EventMouse){
        //this.node.destroy();
        this.gameMaster.getComponent("GameMaster").UpdateScore(this.score);

        //bruh do sumthin before disappearing

        var scoreEffect = cc.instantiate(this.effect);
        scoreEffect.parent = this.node.parent;
        scoreEffect.position = this.node.position;
        scoreEffect.getComponent(cc.Animation).play();

        //Destroy
        this.node.destroy();
        
    }

    FindGameMaster(){
        this.gameMaster = cc.find("GameMaster");

    }

    public setSpeed(value: number){
        this.speed = value;
    }
    public setScale(value: number){
        this.node.scale = value;
    }
    selfDestroy(){
        this.node.destroy();
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //this.node.on(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.node.on(cc.Node.EventType.TOUCH_START, this.onMouseDown, this);
    }
    onDestroy(){
        //this.node.off(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onMouseDown, this);
    }
    start () {
        this.FindGameMaster();
        this.schedule(this.selfDestroy, 10);
    }

    update (dt) {
        this.node.setPositionY(this.node.position.y -= this.speed*dt);
    }
}
