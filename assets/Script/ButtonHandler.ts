
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property
    targetScene: string = "";

    onMouseDown(event: cc.Event.EventMouse){
        cc.director.loadScene(this.targetScene);
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //this.node.on(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.node.on(cc.Node.EventType.TOUCH_START, this.onMouseDown, this);
    }
    onDestroy(){
        //this.node.off(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onMouseDown, this);
    }
    start () {
        this.scheduleOnce(function(){
            console.log("Debug at scheduleOnce button handler: "+this.targetScene);
            //cc.director.preloadScene(String(this.targetScene), function(){
            //    console.log(this.targetScene+" loaded");
            //});
        });

    }

    // update (dt) {}
}
