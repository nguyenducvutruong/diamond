
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    //with this, when mouse clicked on the node with this script, onMouseDown will respond
    onMouseDown(event: cc.Event.EventMouse){
        console.log("Mouse down on the selected node: "+event.currentTarget.name);
        //whether the parent node will also be selected(if the parent also has the event)
        event.bubbles = false;
    }
    onMouseLeave(event: cc.Event.EventMouse){
        console.log("Mouse leaves: "+event.currentTarget.name);
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.on(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.node.on(cc.Node.EventType.MOUSE_LEAVE, this.onMouseLeave, this);
    }
    onDestroy(){
        this.node.off(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
        this.node.off(cc.Node.EventType.MOUSE_LEAVE, this.onMouseLeave, this);
    }

    start () {

    }

    // update (dt) {}
}
