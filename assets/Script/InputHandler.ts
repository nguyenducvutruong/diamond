const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property
    speed = 0;

    @property(cc.Node)
    player: cc.Node = null;
    //public keys: Map<number, boolean> = new Map();

    onKeyDown(event: cc.Event.EventCustom){
        switch(event.keyCode){
            case cc.KEY.d:
                this.player.getComponent('PlayerMovement').speed = this.speed;
                console.log("d pressed");
                break;
            case cc.KEY.a:
                this.player.getComponent('PlayerMovement').speed = -this.speed;
                console.log("a pressed");
                break;
            default:
                console.log("wtf bruh");
                break;
        }

    }

    onKeyUp(event){
        console.log("Key up: "+event.keyCode);
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    onDestroy(){
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start () {

    }

    // update (dt) {}
}
