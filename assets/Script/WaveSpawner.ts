
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    whiteDiamond: cc.Prefab = null;

    @property(cc.Prefab)
    redDiamond: cc.Prefab = null;

    spawnPoints: Array<cc.Vec2> = [new cc.Vec2(-400, 1000), 
        new cc.Vec2(-200, 1000),
        new cc.Vec2(0, 1000),
        new cc.Vec2(200, 1000),
        new cc.Vec2(400, 1000)]; 
    
    spawnPointIndex: number = 0;
    
    @property
    waveRate: number = 2;

    @property
    spawnRate: number = 0.5;

    isLoaded: boolean = false;
    
    //Random.Range, min-max included
    randomRangeInt(min: number, max: number){
        return Math.floor(Math.random()*(max-min+1)+min);
    }
    randomRangeFloat(min:number, max:number){
        return Math.random()*(max-min+1)+min;
    }
    startWave(){
        var random = this.randomRangeInt(0, this.spawnPoints.length-1);
        this.schedule(this.spawnDiamond, this.spawnRate,cc.macro.REPEAT_FOREVER, 0);
    }

    public stopAllSchedules(){
        this.unschedule(this.startWave);
        this.unschedule(this.spawnDiamond);
    }

    random: number = 0;

    spawnDiamond(){
        var diamond = null;
        if(this.random == this.spawnPointIndex){
            diamond = cc.instantiate(this.redDiamond);
        }
        else{
            diamond = cc.instantiate(this.whiteDiamond);
        }

        //set-up
        diamond.parent = cc.find("Canvas/DiamondNest");
        diamond.position = this.spawnPoints[this.spawnPointIndex];
        var diamondSettings = diamond.getComponent("Diamond");
        diamondSettings.setSpeed(this.randomRangeInt(400 ,600));
        diamondSettings.setScale(this.randomRangeFloat(0.7,1)/2);
        this.spawnPointIndex++;
        if(this.spawnPointIndex >= this.spawnPoints.length){
            this.spawnPointIndex = 0;
            this.random = this.randomRangeInt(0, this.spawnPoints.length-1);
        }
    }

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        //Unity InvokeRepeating("functionName", 0 ,invokeRate)
        this.random = this.randomRangeInt(0, this.spawnPoints.length-1);
        this.schedule(this.startWave, this.waveRate);
    }

    // update (dt) {}
}
